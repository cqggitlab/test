// #include <iostream>
// #include <string>
// #include <regex>
 
// int main()
// {
//     // 简单正则表达式匹配
//     std::string fnames[] = {"foo.txt", "bar.txt", "baz.dat", "zoidberg"};
//     std::regex txt_regex("[a-z]+\\.txt");
 
//     for (const auto &fname : fnames) {
//         std::cout << fname << ": " << std::regex_match(fname, txt_regex) << '\n';
//     }   
 
//     // 提取子匹配
//     std::regex base_regex("([a-z]+)\\.txt");
//     std::smatch base_match;
 
//     for (const auto &fname : fnames) {
//         if (std::regex_match(fname, base_match, base_regex)) {
//             // 首个 sub_match 是整个字符串；下个
//             // sub_match 是首个有括号表达式。
//             if (base_match.size() == 2) {
//                 std::ssub_match base_sub_match = base_match[1];
//                 std::string base = base_sub_match.str();
//                 std::cout << fname << " has a base of " << base << '\n';
//             }
//         }
//     }
 
//     // 提取几个子匹配
//     std::regex pieces_regex("([a-z]+)\\.([a-z]+)");
//     std::smatch pieces_match;
 
//     for (const auto &fname : fnames) {
//         if (std::regex_match(fname, pieces_match, pieces_regex)) {
//             std::cout << fname << '\n';
//             for (size_t i = 0; i < pieces_match.size(); ++i) {
//                 std::ssub_match sub_match = pieces_match[i];
//                 std::string piece = sub_match.str();
//                 std::cout << "  submatch " << i << ": " << piece << '\n';
//             }   
//         }   
//     }   
// }


#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
 
void f1(int n)
{
    for (int i = 0; i < 5; ++i)
    {
        std::cout << "正在执行线程1\n";
        ++n;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
 
void f2(int& n)
{
    for (int i = 0; i < 5; ++i)
    {
        std::cout << "正在执行线程2\n";
        ++n;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
 
class foo
{
public:
    void bar()
    {
        for (int i = 0; i < 5; ++i)
        {
            std::cout << "正在执行线程3\n";
            ++n;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    int n = 0;
};
 
class baz
{
public:
    void operator()()
    {
        for (int i = 0; i < 5; ++i)
        {
            std::cout << "正在执行线程4\n";
            ++n;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    int n = 0;
};
 
int main()
{
    int n = 0;
    foo f;
    baz b;
    std::thread t1; // t1 不是线程
    std::thread t2(f1, n + 1); // 按值传递
    std::thread t3(f2, std::ref(n)); // 按引用传递
    std::thread t4(std::move(t3)); // t4 现在运行 f2()。t3 不再是线程
    std::thread t5(&foo::bar, &f); // t5 在对象 f 上运行 foo::bar()
    std::thread t6(b); // t6 在对象 b 的副本上运行 baz::operator()
    t2.join();
    t4.join();
    t5.join();
    t6.join();
    std::cout << "n 的最终值是 " << n << '\n';
    std::cout << "f.n (foo::n) 的最终值是 " << f.n << '\n';
    std::cout << "b.n (baz::n) 的最终值是 " << b.n << '\n';
}